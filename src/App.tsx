import './App.css';
import React, { useReducer } from 'react';
import ReducerContext from './context/state';
import { Details } from './components/details';
import { ToDoContainer } from './components/todocontainer';
import { reducer } from './reducer';
import { StateType } from './types/state';
import { ToDoList, ToDoState } from './types/todo';
export function initial_state(_: any): StateType {
  let obj = {
    lists: [
      {
        id: 0,
        nextId: 3,
        todos: [
          {
            id: 0,
            state: ToDoState.INCOMPLETE,
            title: 'E0',
            createdTime: new Date(),
            dueTime: new Date(),
            priority: 0,
            body: 'blah',
          },
          {
            id: 1,
            state: ToDoState.INCOMPLETE,
            title: 'E1',
            createdTime: new Date(),
            dueTime: new Date(),
            priority: 0,
            body: 'blah',
          },
          {
            id: 2,
            state: ToDoState.INCOMPLETE,
            title: 'E2',
            createdTime: new Date(),
            dueTime: new Date(),
            priority: 0,
            body: 'blah',
          },
        ],
      },
    ] as ToDoList[],
    current: {
      listId: 0,
      todoId: undefined,
    },
  };
  return obj;
}

function App() {
  const [state, dispatch] = useReducer(reducer, {}, initial_state);

  return (
    <ReducerContext.Provider value={{ state, dispatch }}>
      <div className="app-split">
        <div>
          <ToDoContainer />
        </div>
        <Details />
      </div>
    </ReducerContext.Provider>
  );
}

export default App;
