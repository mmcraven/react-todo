import { ToDoState, ToDo, ToDoList } from './todo';
import { Reducer } from 'react';
import { type } from 'os';
interface AddAction {
  type: 'addToDo';
}

interface ChangeBodyAction {
  type: 'bodyChange';
  payload: string;
}

interface ChangeCompletionAction {
  type: 'completionChange';
  payload: ToDoState;
}

interface ChangeDueDateAction {
  type: 'dueDateChange';
  payload: Date;
}

interface ChangeTitleAction {
  type: 'titleChange';
  payload: string;
}

interface ClickedTodoAction {
  type: 'todoClicked';
  payload: {
    todoId: number;
    listId: number;
  };
}
interface DeleteAction {
  type: 'delete';
}
interface SwapAction {
  type: 'swapIndex';
  payload: [number, number];
}
interface ReorderAction {
  type: 'reorder';
  index: [number, number];
}

type ActionType =
  | AddAction
  | ChangeBodyAction
  | ChangeCompletionAction
  | ChangeDueDateAction
  | ChangeTitleAction
  | ClickedTodoAction
  | DeleteAction
  | SwapAction
  | ReorderAction;

interface StateType {
  lists: ToDoList[];
  current: {
    listId: number | undefined;
    todoId: number | undefined;
  };
}

export type { ActionType, StateType };
