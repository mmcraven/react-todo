enum ToDoState {
  INCOMPLETE,
  COMPLETE,
}

interface Reorderable {
  index: number;
  updatePosition: (index: number, position: any) => any;
  updateOrder: (index: number, offset: any) => void;
}

interface ToDo {
  id: number;
  state: ToDoState;
  title: string;
  body: string;
  createdTime: Date;
  dueTime: Date;
  priority: number;
}

interface ToDoList {
  id: number;
  nextId: number;
  name: string;
  todos: ToDo[];
}

type TaskItem = Reorderable & ToDo;
export { ToDoState };
export type { ToDo, ToDoList, Reorderable, TaskItem };
