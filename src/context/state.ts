import { createContext } from "react"
import { ActionType, StateType } from "../types/state";

const ReducerContext = createContext< {state:StateType, dispatch:React.Dispatch<ActionType>}|undefined>(undefined)

export default ReducerContext;