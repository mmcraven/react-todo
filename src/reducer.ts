import { ActionType, StateType } from './types/state';
import { ToDo, ToDoList, ToDoState } from './types/todo';

function cloneTodos(state: StateType): ToDoList[] {
  return state.lists.map((e) => {
    return {
      ...e,
      todos: e.todos.map((f) => {
        return f;
      }),
    };
  });
}
export function reducer(state: StateType, action: ActionType) {
  let newTasks: ToDo[];
  let todos: ToDoList[];
  let activeIndex: number, activeItem: ToDo;
  switch (action.type) {
    case 'addToDo':
      if (state.current.listId === undefined) return state;
      todos = cloneTodos(state);
      newTasks = todos[state.current.listId].todos;
      let newTask: ToDo = {
        id: todos[state.current.listId].nextId++,
        state: ToDoState.INCOMPLETE,
        title: 'New Task',
        body: '',
        createdTime: new Date(),
        dueTime: new Date(),
        priority: 0,
      };
      todos[state.current.listId].todos.push(newTask);
      return { ...state, lists: todos } as StateType;
    case 'bodyChange':
      if (state.current.listId === undefined || state.current.todoId === undefined) return state;
      todos = cloneTodos(state);
      newTasks = todos[state.current.listId].todos;
      activeIndex = newTasks.findIndex((value, _) => value.id === state.current.todoId);
      activeItem = newTasks[activeIndex];
      activeItem.body = action.payload;
      return { ...state, lists: todos } as StateType;
    case 'completionChange':
      if (state.current.listId === undefined || state.current.todoId === undefined) return state;
      todos = cloneTodos(state);
      newTasks = todos[state.current.listId].todos;
      activeIndex = newTasks.findIndex((value, _) => value.id === state.current.todoId);
      activeItem = newTasks[activeIndex];
      activeItem.state = action.payload;
      return { ...state, lists: todos } as StateType;
    case 'delete':
      if (state.current.listId === undefined || state.current.todoId === undefined) return state;
      todos = cloneTodos(state);
      todos[state.current.listId].todos = todos[state.current.listId].todos.filter(
        (value, _) => value.id !== state.current.todoId,
      );
      return { ...state, lists: todos, current: { todoId: undefined, listId: state.current.listId } } as StateType;
    case 'dueDateChange':
      if (state.current.listId === undefined || state.current.todoId === undefined) return state;
      todos = cloneTodos(state);
      newTasks = todos[state.current.listId].todos;
      activeIndex = newTasks.findIndex((value, _) => value.id === state.current.todoId);
      activeItem = newTasks[activeIndex];
      activeItem.dueTime = action.payload;
      return { ...state, lists: todos } as StateType;
    case 'swapIndex':
      if (state.current.listId === undefined) return state;
      todos = cloneTodos(state);
      let _ = todos[state.current.listId].todos;
      let [_0, _1] = action.payload;
      [_[_0], _[_1]] = [_[_1], _[_0]];
      return { ...state, lists: todos } as StateType;
    case 'titleChange':
      if (state.current.listId === undefined || state.current.todoId === undefined) return state;
      todos = cloneTodos(state);
      newTasks = todos[state.current.listId].todos;
      activeIndex = newTasks.findIndex((value, _) => value.id === state.current.todoId);
      activeItem = newTasks[activeIndex];
      activeItem.title = action.payload;
      return { ...state, lists: todos } as StateType;
    case 'todoClicked':
      return {
        ...state,
        current: {
          todoId: action.payload.todoId,
          listId: action.payload.listId,
        },
      };
    default:
      return state;
  }
}
