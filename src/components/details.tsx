import React, { ChangeEvent, useContext } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import ReducerContext from '../context/state';
import { ActionType, StateType } from '../types/state';

import './details.css';

export function Details(props: any) {
  const stateContext = useContext(ReducerContext);
  if (stateContext === undefined) throw Error('In undefined state');
  const typedContext = stateContext as {
    state: StateType;
    dispatch: React.Dispatch<ActionType>;
  };
  if (typedContext.state.current.listId === undefined || typedContext.state.current.todoId === undefined)
    return <div />;
  console.log(typedContext.state.current);
  const todos = typedContext.state.lists[typedContext.state.current.listId].todos;
  const activeIndex = todos.findIndex((value, _) => value.id === typedContext.state.current.todoId);
  const activeItem = todos[activeIndex];
  return (
    <div className="input-group-addon todo-details equalize-width">
      <input
        type="text"
        value={activeItem.title}
        onChange={(e: ChangeEvent<HTMLInputElement>) => {
          typedContext.dispatch({
            type: 'titleChange',
            payload: e.target.value,
          });
        }}
      />
      <p />
      {/*https://www.npmjs.com/package/react-datepicker*/}
      <div className="date-align">
        <label htmlFor="createDate">Creation date</label>
        <input
          className="createDate"
          type="text"
          readOnly={true}
          value={activeItem.createdTime.toLocaleDateString()}
          name="createDate"
        />
      </div>
      <div className="date-align">
        <label htmlFor="dueDate">Due date</label>
        <DatePicker
          selected={activeItem.dueTime}
          name="dueDate"
          onChange={(e: Date) => {
            typedContext.dispatch({ type: 'dueDateChange', payload: e });
          }}
        />
      </div>

      <textarea
        value={activeItem.body}
        onChange={(e: ChangeEvent<HTMLTextAreaElement>) => {
          typedContext.dispatch({
            type: 'bodyChange',
            payload: e.target.value,
          });
        }}
      />
      <p />
      <button
        onClick={() => {
          typedContext.dispatch({ type: 'delete' });
        }}
      >
        Delete ToDo
      </button>
    </div>
  );
}
