import ToDoItem from './todoitem';
import { ToDo } from '../types/todo';
import { usePositionReorder } from '../motion/use-position-reorder';
import './todocontainer.css';
import ReducerContext from '../context/state';
import { ActionType, StateType } from '../types/state';
import { useContext, useState } from 'react';
const create_todo = (props: ToDo, index: number, updatePosition: any, updateOrder: any) => {
  return (
    <ToDoItem
      {...props}
      key={props.id}
      listId={0}
      index={index}
      updatePosition={updatePosition}
      updateOrder={updateOrder}
    />
  );
};

export function ToDoContainer(props: any) {
  const stateContext = useContext(ReducerContext);
  const [query, setQuery] = useState('');
  if (stateContext === undefined) throw Error('In undefined state');
  const typedContext = stateContext as { state: StateType; dispatch: React.Dispatch<ActionType> };
  let list: ToDo[];
  if (typedContext.state.current.listId === undefined) list = [];
  else list = typedContext.state.lists[typedContext.state.current.listId].todos;
  let [updatePosition, updateOrder] = usePositionReorder();
  return (
    <div className="todo-container">
      <button
        onClick={(e) => {
          e.stopPropagation();
          typedContext.dispatch({ type: 'addToDo' });
        }}
      >
        {'Add ToDo'}
      </button>
      <div>
        <input
          className="searchBar"
          type="text"
          value={query}
          onChange={(e) => {
            setQuery(e.target.value);
          }}
        />
        {list
          .filter((value) => {
            return query === '' || value.title.match(query) || value.body.match(query);
          })
          .map((value, index: any) => create_todo(value, index, updatePosition, updateOrder))}
      </div>
    </div>
  );
}
