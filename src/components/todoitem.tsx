import './todoitem.css';
import { TaskItem, ToDoState } from '../types/todo';
import { motion } from 'framer-motion';
import { useMeasurePosition } from '../motion/use-measure-position';
import React, { ChangeEvent, useContext } from 'react';
import ReducerContext from '../context/state';
import { ActionType, StateType } from '../types/state';

function shoutOut(dispatch: React.Dispatch<ActionType>, props: any) {
  console.log(props.id);
  dispatch({
    type: 'todoClicked',
    payload: { todoId: props.id, listId: props.listId },
  });
}

export default function ToDoItem(props: TaskItem & { listId: number }) {
  const [isdragged, setIsDragged] = React.useState(false);
  const stateContex = useContext(ReducerContext);
  const itemRef = useMeasurePosition((pos: any) => props.updatePosition(props.index, pos));
  if (stateContex === undefined) throw Error('In undefined state');
  const typedContext = stateContex as {
    state: StateType;
    dispatch: React.Dispatch<ActionType>;
  };
  const suffix = props.state === ToDoState.COMPLETE ? 'complete' : 'incomplete';
  return (
    <motion.div
      className="todo-item"
      style={{
        zIndex: isdragged ? 2 : 1,
      }}
      dragConstraints={{
        top: 0,
        bottom: 0,
      }}
      dragElastic={1}
      layout
      ref={itemRef}
      onDragStart={() => setIsDragged(true)}
      onDragEnd={() => setIsDragged(false)}
      animate={{
        scale: isdragged ? 1.05 : 1,
      }}
      onViewportBoxUpdate={(_: any, delta: any) => {
        isdragged && props.updateOrder(props.index, delta.y.translate);
      }}
      drag="y"
    >
      <input
        type="checkbox"
        className="todo-check"
        checked={props.state === ToDoState.COMPLETE}
        onChange={(e: ChangeEvent<HTMLInputElement>) => {
          typedContext.dispatch({
            type: 'todoClicked',
            payload: { todoId: props.id, listId: props.listId },
          });
          typedContext.dispatch({
            type: 'completionChange',
            payload: e.target.checked ? ToDoState.COMPLETE : ToDoState.INCOMPLETE,
          });
        }}
      />
      <div
        className={'todo-text-' + suffix}
        onClick={() => {
          shoutOut(typedContext.dispatch, props);
        }}
      >
        <input
          type="text"
          onChange={(e: ChangeEvent<HTMLInputElement>) => {
            typedContext.dispatch({
              type: 'titleChange',
              payload: e.target.value,
            });
          }}
          value={props.title}
          className={'todo-title-' + suffix}
        />
        <div className="todo-date">{props.dueTime.toLocaleDateString()}</div>
      </div>
    </motion.div>
  );
}
